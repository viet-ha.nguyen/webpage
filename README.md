1. ``pull``,
1. modify files ``.html`` and ``.css`` from folder ``public/``,
1. ``add``+``commit``+``push``,
1. (after a few seconds) see the result at <http://viet-ha.nguyen.gitlab.io/webpage/> (deployment report in ``CI/CD > Pipelines``).

``.gitlab-ci.yml`` launches automatically the website update (_aka_ deployment).

